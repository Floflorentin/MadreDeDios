package fr.carbon.peru.orientation;

import fr.carbon.peru.Coordinates;

public class South implements Orientation {
    @Override
    public Orientation turnRight() {
        return Orientation.WEST;
    }

    @Override
    public Orientation turnLeft() {
        return Orientation.EAST;
    }

    @Override
    public Coordinates forward(Coordinates coordinates) {
        return new Coordinates(coordinates.x(), coordinates.y() + 1);
    }

    @Override
    public String toString() {
        return "S";
    }
}
