package fr.carbon.peru.orientation;

import fr.carbon.peru.Coordinates;

public class West implements Orientation {
    @Override
    public Orientation turnRight() {
        return Orientation.NORTH;
    }

    @Override
    public Orientation turnLeft() {
        return Orientation.SOUTH;
    }

    @Override
    public Coordinates forward(Coordinates coordinates) {
        return new Coordinates(coordinates.x() - 1, coordinates.y());
    }

    @Override
    public String toString() {
        return "O";
    }
}
