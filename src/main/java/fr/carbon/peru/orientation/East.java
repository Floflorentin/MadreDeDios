package fr.carbon.peru.orientation;

import fr.carbon.peru.Coordinates;

public class East implements Orientation {
    @Override
    public Orientation turnRight() {
        return Orientation.SOUTH;
    }

    @Override
    public Orientation turnLeft() {
        return Orientation.NORTH;
    }

    @Override
    public Coordinates forward(Coordinates coordinates) {
        return new Coordinates(coordinates.x() + 1, coordinates.y());
    }

    @Override
    public String toString() {
        return "E";
    }
}
