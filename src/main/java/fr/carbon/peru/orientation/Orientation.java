package fr.carbon.peru.orientation;

import fr.carbon.peru.Coordinates;

public interface Orientation {
    // Orientation implementation should be static
    Orientation EAST = new East();
    Orientation NORTH = new North();
    Orientation WEST = new West();
    Orientation SOUTH = new South();

    Orientation turnRight();
    Orientation turnLeft();
    Coordinates forward(Coordinates coordinates);
}
