package fr.carbon.peru;

public record Coordinates(int x, int y) {
}
