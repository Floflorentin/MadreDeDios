package fr.carbon.peru.operator;

import fr.carbon.peru.operator.command.Command;
import fr.carbon.peru.operator.command.Forward;
import fr.carbon.peru.operator.command.TurnLeft;
import fr.carbon.peru.operator.command.TurnRight;

import java.util.List;
import java.util.Objects;

public class CommandParser {
    private CommandParser() {
        throw new IllegalStateException("Utility class shouldn't be created.");
    }
    public static List<Command> parseCommand(List<CommandsHandler> commandsToParse) {
        return commandsToParse.stream().map(nextCommand ->
                        new CommandRecord(nextCommand.id(), nextCommand.operation().poll()))
                .filter(newCommand -> Objects.nonNull(newCommand.command()))
                .map(newCommand -> switch (newCommand.command()) {
                    case 'G' -> new TurnLeft(newCommand.id());
                    case 'D' -> new TurnRight(newCommand.id());
                    case 'A' -> new Forward(newCommand.id());
                    default -> throw new IllegalArgumentException("Invalid command");// x, y => coordinates
                })
                .toList();
    }
}
