package fr.carbon.peru.operator;

import java.util.LinkedList;

// operation should be a lambda or complete object (Visitor ?)
// Should use commands conception patron
public record CommandsHandler(int id, LinkedList<Character> operation) {}
