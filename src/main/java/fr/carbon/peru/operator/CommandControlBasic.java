package fr.carbon.peru.operator;

import fr.carbon.peru.Adventurer;
import fr.carbon.peru.operator.command.Command;

import java.util.*;

// State
public class CommandControlBasic implements CommandControl {
    private final ArrayList<CommandsHandler> commands      = new ArrayList<>();
    private final ArrayList<Adventurer> adventurers = new ArrayList<>();
    private int longestCommand = 0;

    @Override
    public int addAdventurer(Adventurer adventurer) {
        Objects.requireNonNull(adventurer);
        adventurers.add(adventurer);
        return adventurers.indexOf(adventurer); // Efficient ? or put private field count
    }

    @Override
    public Optional<Adventurer> getAdventurer(int id) {
        if(id < 0 || id > adventurers.size()) {
            throw new IndexOutOfBoundsException("Invalid id.");
        }
        return Optional.ofNullable(adventurers.get(id));
    }

    @Override
    public void addCommand(int id, String command) {
        // Should depends on another classes
        Objects.checkIndex(id, this.adventurers.size());
        Objects.requireNonNull(command);
        if(id < this.commands.size()) {
            throw new IllegalStateException("Command already set for user " + id);
        }
        this.longestCommand = Math.max(command.length(), longestCommand);
        LinkedList<Character> commandsLinkedL = new LinkedList<>();
        for(char c : command.toCharArray()) {
            commandsLinkedL.addLast(c);
        }
        this.commands.add(id, new CommandsHandler(id, commandsLinkedL));
    }

    @Override
    public List<Command> getNextCommand() {
        // State ? Builder ? Constructor ?
        // Empty command should be removed
        return CommandParser.parseCommand(this.commands);
    }

    /*
     * List command
     * Interface command :
     *      - Apply
     *      ?
     * Record command nul actuellement, autant hashmap aventurer -> linkedList< c >
     * Remplacer aventurer avec object aventurer plus ID (id vraiment utile ?)
     * List de Record avec aventurer (?), id, et linkedList C
     *
     * Add state in CommandControlBasic
     */
}
