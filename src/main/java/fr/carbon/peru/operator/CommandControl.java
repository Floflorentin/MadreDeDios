package fr.carbon.peru.operator;

import fr.carbon.peru.Adventurer;
import fr.carbon.peru.operator.command.Command;

import java.util.List;
import java.util.Optional;

public interface CommandControl {
    int addAdventurer(Adventurer adventurer);
    Optional<Adventurer> getAdventurer(int id);
    void addCommand(int id, String command);
    List<Command> getNextCommand();
}
