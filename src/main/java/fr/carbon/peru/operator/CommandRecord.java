package fr.carbon.peru.operator;

public record CommandRecord(int id, Character command) {}
