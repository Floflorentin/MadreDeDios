package fr.carbon.peru.operator.command;

import fr.carbon.peru.map.MapController;
import fr.carbon.peru.operator.CommandControl;

public abstract class Command {
    public int id;

    Command(int id) {
        this.id = id;
    }

    public abstract void execute(CommandControl commandControl,
                                 MapController mapController);
}
