package fr.carbon.peru.operator.command;

import fr.carbon.peru.map.MapController;
import fr.carbon.peru.operator.CommandControl;

public class TurnLeft extends Command {
    public TurnLeft(int id) {
        super(id);
    }

    @Override
    public void execute(CommandControl commandControl, MapController mapController) {
        commandControl.getAdventurer(this.id).orElseThrow().turnLeft();
    }
}
