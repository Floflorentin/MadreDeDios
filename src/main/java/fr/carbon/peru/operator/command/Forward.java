package fr.carbon.peru.operator.command;

import fr.carbon.peru.map.MapController;
import fr.carbon.peru.operator.CommandControl;

public class Forward extends Command {
    public Forward(int id) {
        super(id);
    }

    @Override
    public void execute(CommandControl commandControl, MapController mapController) {
        var adv = commandControl.getAdventurer(this.id).orElseThrow();
        var pos = adv.testForward();
        if(mapController.moveAdventurerOrFalse(adv.getCoordinates().x(),
                adv.getCoordinates().y(),
                pos.x(),
                pos.y())) {
            adv.forward(pos);
            if(mapController.getTreasureOrFalse(pos.x(), pos.y())) {
                adv.addTreasure();
            }
        }
    }
}
