package fr.carbon.peru;

import java.io.*;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.carbon.peru.map.MapController;
import fr.carbon.peru.map.Mountain;
import fr.carbon.peru.operator.CommandControl;
import fr.carbon.peru.orientation.Orientation;

// Should be static or should implement an interface
public class Parser {
    private final String path;
    private static final Logger LOGGER = Logger.getGlobal();

    public Parser(String path) {
        Objects.requireNonNull(path);
        this.path = path;
    }

    public void parse(MapController mapController, CommandControl commandControl) {
        File file = new File(path);
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;

            while ((line = br.readLine()) != null) {
                if(line.startsWith("C")) {
                    var mapSize = line.split("-");
                    int x = stringToInt(mapSize[1]);
                    int y = stringToInt(mapSize[2]);
                    mapController.setDimension(x, y);
                    break;
                }
            }

            loopAndConstructTreasureMap(mapController, commandControl, br);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private void loopAndConstructTreasureMap(MapController mapController, CommandControl commandControl, BufferedReader br) {
        try {
            String line;
            while ((line = br.readLine()) != null) {
                switch (line.charAt(0)) {
                    case 'M' -> addMountain(line, mapController);
                    case 'T' -> addTreasure(line, mapController);
                    case 'A' -> addAdventurer(line, mapController, commandControl);
                    default -> LOGGER.log(Level.WARNING, "Invalid command while parsing.");
                }
            }

        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private void addAdventurer(String line, MapController mapController, CommandControl commandControl) {
        var adventurer = line.split("-");
        var name = adventurer[1].trim();
        var direction = switch (adventurer[4].trim()) {
            case "S" -> Orientation.SOUTH;
            case "E" -> Orientation.EAST;
            case "N" -> Orientation.NORTH;
            case "O" -> Orientation.WEST;
            default -> throw new IllegalArgumentException("Incorrect orientation.");
        };
        var op = adventurer[5].trim();
        var coordinates = new Coordinates(stringToInt(adventurer[2]), stringToInt(adventurer[3]));
        var adv = new Adventurer(name, direction, coordinates);
        if(!mapController.addAdventurerOrFalse(coordinates.x(), coordinates.y(),
                adv)) {
            LOGGER.log(Level.WARNING, "Incorrect position for adventurer.");
            return;
        }
        commandControl.addCommand(commandControl.addAdventurer(adv), op);
    }

    private void addTreasure(String line, MapController mapController) {
        var treasurePosAndNumber = line.split("-");
        mapController.addTreasure(stringToInt(treasurePosAndNumber[1]),
                stringToInt(treasurePosAndNumber[2]),
                stringToInt(treasurePosAndNumber[3]));
    }

    private void addMountain(String line, MapController mapController) {
        var mountainPos = line.split("-");
        if(!mapController.addGroundEntityOrFalse(stringToInt(mountainPos[1]),
                stringToInt(mountainPos[2]),
                new Mountain())) {
            LOGGER.log(Level.WARNING, "Incorrect position for mountain while parsing.");
        }
    }

    private int stringToInt(String strToTransform) {
        return Integer.parseInt(strToTransform.trim());
    }
}
