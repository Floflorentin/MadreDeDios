package fr.carbon.peru.map;

import fr.carbon.peru.Adventurer;

import java.util.List;
import java.util.Map;

public interface MapController {
    void setDimension(int x, int y);
    void addTreasure(int x, int y, int n);

    boolean getTreasureOrFalse(int x, int y);

    boolean addGroundEntityOrFalse(int x, int y, GroundEntity groundEntity);
    boolean addAdventurerOrFalse(int x, int y, Adventurer adventurer);
    boolean moveAdventurerOrFalse(int x, int y, int newX, int newY);

    int getX();
    int getY();
    Map<Integer, Integer> getTreasures();
    Map<Integer, Adventurer> getAdventurersPos();
    List<Integer> getMap();
}
