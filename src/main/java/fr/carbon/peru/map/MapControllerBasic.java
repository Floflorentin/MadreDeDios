package fr.carbon.peru.map;

import fr.carbon.peru.Adventurer;

import java.util.*;

// Add state
public class MapControllerBasic implements MapController {
    private final List<Integer> mountainsPos = new ArrayList<>(); // Sorted immutable list
    private final Map<Integer, Adventurer> adventurersPos = new HashMap<>();
    private final Map<Integer, Integer> treasures = new HashMap<>();
    private State state = State.INIT;

    private int sizeX = 0;
    private int sizeY = 0;

    @Override
    public void setDimension(int sizeX, int sizeY) {
        if(state != State.INIT) {
            throw new IllegalStateException("Dimension already set.");
        }
        if(sizeX <= 0 || sizeY <= 0) {
            throw new IllegalArgumentException("Size of map shouldn't be equals or inferior to 0.");
        }

        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.state = State.IN_PROGRESS;
    }

    @Override
    public void addTreasure(int x, int y, int n) {
        if(this.state != State.IN_PROGRESS) {
            throw new IllegalStateException("Map size not defined yet or game already started while adding trasure.");
        }
        if(n <= 0) {
            throw new IllegalArgumentException("Number of treasure shouldn't be equals or inferior to 0.");
        }
        Objects.checkIndex(x, sizeX);
        Objects.checkIndex(y, sizeY);
        // Check if pos is empty
        this.treasures.put(getPos(x, y), n);
    }

    private Integer getPos(int x, int y) {
        return this.sizeX * y + x;
    }

    @Override
    public boolean addGroundEntityOrFalse(int x, int y, GroundEntity groundEntity) {
        if(this.state != State.IN_PROGRESS) {
            throw new IllegalStateException("Map size not defined yet or game already started while adding ground entity.");
        }
        Objects.requireNonNull(groundEntity);
        if(checkIfEmpty(x, y)) {
            return this.mountainsPos.add(this.getPos(x, y));
        }
        return false;
    }

    @Override
    public boolean addAdventurerOrFalse(int x, int y, Adventurer adventurer) {
        if(this.state != State.IN_PROGRESS) {
            throw new IllegalStateException("Map size not defined yet or game already started.");
        }
        Objects.requireNonNull(adventurer);
        if(checkIfEmpty(x, y)) {
            this.adventurersPos.put(getPos(x, y), adventurer);
            return true;
        }
        return false;
    }

    private boolean checkIfEmpty(int x, int y) {
        Objects.checkIndex(x, sizeX);
        Objects.checkIndex(y, sizeY);
        var pos = this.getPos(x, y);
        return !(this.mountainsPos.contains(pos) ||
                this.adventurersPos.containsKey(pos) ||
                this.treasures.containsKey(pos));
    }

    @Override
    public boolean moveAdventurerOrFalse(int x, int y, int newX, int newY) {
        if(this.state == State.IN_PROGRESS) {
            this.state = State.READY;
        } else if(this.state != State.READY) {
            throw new IllegalStateException("Game not started yet, can't move adventurer.");
        }
        Objects.checkIndex(x, this.sizeX);
        Objects.checkIndex(y, this.sizeY);

        try {
            Objects.checkIndex(newX, this.sizeX);
            Objects.checkIndex(newY, this.sizeY);
        } catch (IndexOutOfBoundsException i) {
            return false;
        }
        if(!this.adventurersPos.containsKey(getPos(x, y)) ||
                !checkIfPossible(newX, newY)) {
            return false;
        }
        this.adventurersPos.put(getPos(newX, newY), this.adventurersPos.remove(getPos(x, y)));
        return true;
    }

    @Override
    public boolean getTreasureOrFalse(int x, int y) {
        if(this.state != State.READY) {
            throw new IllegalStateException("Game not ready yet.");
        }

        int pos = getPos(x,y);
        if(this.treasures.containsKey(pos)) {
            int n = this.treasures.get(pos);

            if(n <= 1) {
                this.treasures.remove(pos);
            } else {
                this.treasures.put(pos, --n);
            }
            return true;
        }
        return false;
    }

    @Override
    public int getX() {
        return this.sizeX;
    }

    private boolean checkIfPossible(int x, int y) {
        int pos = getPos(x, y);
        return !this.adventurersPos.containsKey(pos) && !this.mountainsPos.contains(pos);
    }

    @Override
    public int getY() {
        return this.sizeY;
    }
    @Override
    public Map<Integer, Integer> getTreasures() {
        return Map.copyOf(this.treasures);
    }

    @Override
    public List<Integer> getMap() {
        return List.copyOf(this.mountainsPos);
    }

    @Override
    public Map<Integer, Adventurer> getAdventurersPos() {
        return Map.copyOf(this.adventurersPos);
    }

    private enum State {
        INIT, IN_PROGRESS, READY
    }
}
