package fr.carbon.peru;

import fr.carbon.peru.orientation.Orientation;

public class Adventurer {
    private final String name;
    private Orientation orientation; // Array of static orientation
    private Coordinates coordinates; // Shouldn't contain this
    private int treasure = 0;

    public Adventurer(String name, Orientation orientation, Coordinates coordinates) {
        this.name = name;
        this.orientation = orientation;
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "Name : " + name + " <-> Direction : " + orientation + " <-> Coordinates : " + coordinates;
    }


    public Coordinates getCoordinates() {
        return coordinates;
    }

    public Orientation getOrientation() {
        return orientation;
    }
    public String getName() {
        return name;
    }

    public void turnRight() {
        this.orientation = this.orientation.turnRight();
    }

    public void turnLeft() {
        this.orientation = this.orientation.turnLeft();
    }

    public Coordinates testForward() {
        return this.orientation.forward(this.coordinates);
    }


    public void forward() {
        this.coordinates = this.orientation.forward(this.coordinates);
    }
    public void forward(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public void addTreasure() {
        this.treasure++;
    }

    public int getTreasure() {
        return this.treasure;
    }
}

