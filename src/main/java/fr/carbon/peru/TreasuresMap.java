package fr.carbon.peru;
import fr.carbon.peru.map.MapController;
import fr.carbon.peru.operator.command.Command;
import fr.carbon.peru.operator.CommandControl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TreasuresMap {
    private final MapController mapController;
    private final CommandControl commandControl;
    // Should be another class view
    private final Logger treasureLogger = Logger.getGlobal();

    public TreasuresMap(MapController mapController, CommandControl commandControl) {
        this.mapController = mapController;
        this.commandControl = commandControl;
    }

    public void constructMap(Parser parser) {
        try {
            parser.parse(mapController, commandControl);
        } catch (Exception e) {
            treasureLogger.log(Level.SEVERE, e.getMessage(), e.getCause());
        }
    }

    public void treasureHuntOpen(List<Command> commands) {
        commands.forEach(command -> command.execute(this.commandControl, this.mapController));
    }

    public void opening() {
        List<Command> nextCommands;
        while (!(nextCommands = this.commandControl.getNextCommand()).isEmpty()) {
            treasureHuntOpen(nextCommands);
        }
    }

    // Should be called directly in controller
    public void output(String output) {
        List<String> lines = new ArrayList<>();
        int x = this.mapController.getX();
        int y = this.mapController.getY();
        lines.add("C - " + x + " - " + y);
        this.mapController.getMap()
                .stream()
                .map(i -> new Coordinates(i%x, i/x))
                .forEach(c -> lines.add("M - " + c.x() + " - " + c.y()));

        this.mapController.getTreasures().forEach((key, value) -> {
            int posX = key;
            lines.add("T - " + (posX % x)+ " - " + (posX / x) + " - " + value);
        });

        this.mapController.getAdventurersPos().forEach((key, value) -> {
            int posX = key;
            StringBuilder str = new StringBuilder();
            str.append("A - ")
                    .append(value.getName())
                    .append(" - ")
                    .append((posX % x))
                    .append(" - ")
                    .append(posX / x)
                    .append(" - ")
                    .append(value.getOrientation())
                    .append(" - ")
                    .append(value.getTreasure());
            lines.add(str.toString());
        });

        Path file = Paths.get(output);
        try {
            Files.write(file, lines, StandardCharsets.UTF_8);
        } catch (IOException e) {
            treasureLogger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

}
