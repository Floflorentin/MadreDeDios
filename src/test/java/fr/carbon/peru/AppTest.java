package fr.carbon.peru;

import fr.carbon.peru.map.MapControllerBasic;
import fr.carbon.peru.map.Mountain;
import fr.carbon.peru.operator.CommandControlBasic;
import fr.carbon.peru.orientation.Orientation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    private static final String FILE_BASIC = "src/main/resources/input/basicMap.txt";

    // Should more test errors

    @Test
    @DisplayName("Test adventurer movement with mountain.")
    void testAdventurerMovementMountain() {
        var ad = new Adventurer("Anna", Orientation.SOUTH, new Coordinates(1, 1));
        var map = new MapControllerBasic();
        map.setDimension(3, 3);

        assertAll(
                () -> assertTrue(map.addAdventurerOrFalse(1, 1, ad)),
                () -> assertFalse(map.addGroundEntityOrFalse(1, 1, new Mountain())),
                () -> assertTrue(map.addGroundEntityOrFalse(1, 0, new Mountain())),
                () -> assertFalse(map.moveAdventurerOrFalse(1, 1, 1, 0)),
                () -> assertTrue(map.moveAdventurerOrFalse(1, 1, 1, 2))
        );
    }

    @Test
    @DisplayName("Test adventurers movement with mountain and other adventurers")
    void testAdventurersMovementMountain() {
        var anna = new Adventurer("Anna", Orientation.SOUTH, new Coordinates(0, 0));
        var bob = new Adventurer("Bob", Orientation.SOUTH, new Coordinates(1, 0));
        var map = new MapControllerBasic();
        map.setDimension(3, 3);

        assertAll(
                () -> assertTrue(map.addAdventurerOrFalse(0, 0, anna)),
                () -> assertFalse(map.addAdventurerOrFalse(0, 0, bob)),
                () -> assertTrue(map.addAdventurerOrFalse(1, 0, bob)),
                () -> assertTrue(map.addGroundEntityOrFalse(1, 1, new Mountain())),
                () -> assertFalse(map.moveAdventurerOrFalse(0, 0, 1, 0)),
                () -> assertFalse(map.moveAdventurerOrFalse(1, 0, 0, 0)),
                () -> assertFalse(map.moveAdventurerOrFalse(1, 0, 1, 1)),
                () -> assertFalse(map.moveAdventurerOrFalse(1, 1, 2, 2)),
                () -> assertTrue(map.moveAdventurerOrFalse(0, 0, 2, 2)),
                () -> assertTrue(map.moveAdventurerOrFalse(1, 0, 0, 0))
        );
    }

    @Test
    @DisplayName("Test adventurer movement from command")
    void testAdventurerMovementCommand() {
        var map = new MapControllerBasic();
        var commandController = new CommandControlBasic();
        var treasureController = new TreasuresMap(map, commandController);
        var ad = new Adventurer("Anna", Orientation.SOUTH, new Coordinates(0, 0));
        map.setDimension(3, 3);
        map.addAdventurerOrFalse(0,0, ad);
        commandController.addAdventurer(ad);

        var command = "AAGAA";
        commandController.addCommand(0, command);
        IntStream.range(0, command.length())
                .forEach(i -> treasureController.treasureHuntOpen(commandController.getNextCommand()));
        assertEquals(new Coordinates(2, 2), ad.getCoordinates());
    }

    @Test
    @DisplayName("Test adventurers movement with mountain and other adventurers and command")
    void testAdventurersMovementMountainWithCommand() {
        var map = new MapControllerBasic();
        var commandController = new CommandControlBasic();
        var treasureController = new TreasuresMap(map, commandController);
        var anna = new Adventurer("Anna", Orientation.SOUTH, new Coordinates(0, 0));
        var bob = new Adventurer("Bob", Orientation.SOUTH, new Coordinates(1, 0));
        var charlie = new Adventurer("Charlie", Orientation.SOUTH, new Coordinates(1, 1));
        map.setDimension(3, 3);
        map.addAdventurerOrFalse(0,0, anna);
        map.addAdventurerOrFalse(1,0, bob);
        map.addAdventurerOrFalse(1,1, charlie);
        commandController.addAdventurer(anna);
        commandController.addAdventurer(bob);
        commandController.addAdventurer(charlie);

        var annaCommand = "AAGAA";
        var bobCommand = "ADADAA";
        var charlieCommand = "DDAA";
        commandController.addCommand(0, annaCommand);
        commandController.addCommand(1, bobCommand);
        commandController.addCommand(2, charlieCommand);

        IntStream.range(0, bobCommand.length())
                .forEach(i -> treasureController.treasureHuntOpen(commandController.getNextCommand()));

        assertAll(
                () -> assertEquals(new Coordinates(2,2), anna.getCoordinates()),
                () -> assertEquals(new Coordinates(0,0), bob.getCoordinates()),
                () -> assertEquals(new Coordinates(1,0), charlie.getCoordinates())
        );
    }

    @Test
    @DisplayName("Complete test")
    void testCompleteMapParsingMovementCommand() throws IOException {
        var map = new MapControllerBasic();
        var treasureMap = new TreasuresMap(map, new CommandControlBasic());
        treasureMap.constructMap(new Parser(FILE_BASIC));
        treasureMap.opening();
        treasureMap.output(App.OUTPUT_FILE);

        var result = App.OUTPUT_FILE;
        var expected = "src/test/resources/output/expected.txt";

        byte[] f1 = Files.readAllBytes(Path.of(result));
        byte[] f2 = Files.readAllBytes(Path.of(expected));

        assertArrayEquals(f1, f2);
    }

    @ParameterizedTest
    @ValueSource(ints = { 10, 50, 100, 500, 1000, 5000, 10000, 50000 })
    @DisplayName("H U G E  RANDOM TEST")
    void hugeRandomTest(int iteration) {
        var map = new MapControllerBasic();
        var command = new CommandControlBasic();
        var treasureMap = new TreasuresMap(map, command);
        var x = 1000;
        var y = 1500;
        map.setDimension(x,y);
        
        var random = new Random();
        var ref = new Object() {
            int numberOfAdventurer = 0;
            int numberOfMountain = 0;
            int numberOfTreasure = 0;
        };
        IntStream.range(0, iteration).forEach(i -> {
            if(i % 2 == 0) {
                var tmpAdv = new Adventurer("xXUSER42#" + i, switch (random.nextInt(4)) {
                    case 1 -> Orientation.NORTH;
                    case 2 -> Orientation.EAST;
                    case 3 -> Orientation.WEST;
                    default -> Orientation.SOUTH;
                }, new Coordinates(random.nextInt(x), random.nextInt(y)));
                if(map.addAdventurerOrFalse(tmpAdv.getCoordinates().x(), tmpAdv.getCoordinates().y(), tmpAdv)) {
                    ref.numberOfAdventurer++;
                }
                command.addCommand(command.addAdventurer(tmpAdv), "AAAAAAAAAAAAAAAAAAAAAAAAAAAGGDDADADAGGDADA");
            } else {
                if (i % 3 == 0) {
                    var treasure = random.nextInt(40) + 1;
                    ref.numberOfTreasure += treasure;
                    map.addTreasure(random.nextInt(x), random.nextInt(y), treasure);
                } else {
                    if(map.addGroundEntityOrFalse(random.nextInt(x), random.nextInt(y), new Mountain())) {
                        ref.numberOfMountain++;
                    }
                }
            }
        });
        treasureMap.opening();
        System.out.println("Iteration : " + iteration);
        System.out.println("Adventurer : " + ref.numberOfAdventurer);
        System.out.println("Mountain : " + ref.numberOfMountain);
        System.out.println("Treasure : " + ref.numberOfTreasure);

        var sumAdventurerTreasure = map.getAdventurersPos().values().stream().mapToInt(Adventurer::getTreasure).sum();
        var sumTreasure = map.getTreasures().values().stream().mapToInt(i -> i).sum();
        System.out.println("Adventurer treasure : " + sumAdventurerTreasure);
        System.out.println("Treasure in map : " + sumTreasure);

        assertEquals(map.getMap().size(), ref.numberOfMountain);
        assertEquals(map.getAdventurersPos().size(), ref.numberOfAdventurer);
        assertEquals(sumAdventurerTreasure + sumTreasure, ref.numberOfTreasure);
    }

}