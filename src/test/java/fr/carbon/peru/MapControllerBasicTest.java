package fr.carbon.peru;

import fr.carbon.peru.map.MapControllerBasic;
import fr.carbon.peru.map.Mountain;
import fr.carbon.peru.orientation.Orientation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MapControllerBasicTest {

    @Test
    void setDimension() {
        var map = new MapControllerBasic();
        assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> map.setDimension(10, -1)),
                () -> assertThrows(IllegalArgumentException.class, () -> map.setDimension(-1, 10))
                );
        map.setDimension(10, 1000);
        assertAll(
                () -> assertEquals(10, map.getX()),
                () -> assertEquals(1000, map.getY())
        );
    }

    @Test
    void testState() {
        var map = new MapControllerBasic();
        assertAll(
                () -> assertThrows(IllegalStateException.class, () -> map.addTreasure(0, 0, 1)),
                () -> assertThrows(IllegalStateException.class, () -> map.addAdventurerOrFalse(0, 0, new Adventurer("Anna", null, null))),
                () -> assertThrows(IllegalStateException.class, () -> map.getTreasureOrFalse(0, 1)),
                () -> assertThrows(IllegalStateException.class, () -> map.addGroundEntityOrFalse(0, 0, new Mountain())),
                () -> assertThrows(IllegalStateException.class, () -> map.moveAdventurerOrFalse(0, 0, 0, 1))
        );
        map.setDimension(3,3);
        assertAll(
                () -> assertThrows(IllegalStateException.class, () -> map.setDimension(3,5)),
                () -> assertThrows(IllegalStateException.class, () -> map.getTreasureOrFalse(3,5))
        );
        map.addAdventurerOrFalse(0, 0, new Adventurer("Anna", null, null));
        map.moveAdventurerOrFalse(0, 0, 0, 1);
        assertAll(
                () -> assertThrows(IllegalStateException.class, () -> map.setDimension(3,3)),
                () -> assertThrows(IllegalStateException.class, () -> map.addTreasure(3,3, 1)),
                () -> assertThrows(IllegalStateException.class, () -> map.addAdventurerOrFalse(3,3, new Adventurer("Anna", null, null))),
                () -> assertThrows(IllegalStateException.class, () -> map.addGroundEntityOrFalse(3,3, new Mountain()))
        );
    }

    @Test
    void addOrGetTreasure() {
        var map = new MapControllerBasic();
        map.setDimension(3, 3);
        map.addTreasure(1,2, 1);
        map.addAdventurerOrFalse(0, 0, new Adventurer("Anna", null, null));
        assertAll(
                () -> assertThrows(IndexOutOfBoundsException.class, () -> map.addTreasure(0,-1, 1)),
                () -> assertThrows(IndexOutOfBoundsException.class, () -> map.addTreasure(50,0, 1)),
                () -> assertThrows(IllegalArgumentException.class, () -> map.addTreasure(1, 1, -1)),
                () -> assertTrue(map.moveAdventurerOrFalse(0, 0, 0, 1)),
                () -> assertFalse(map.getTreasureOrFalse(1,1)),
                () -> assertFalse(map.getTreasureOrFalse(1,1)),
                () -> assertTrue(map.getTreasureOrFalse(1,2)),
                () -> assertFalse(map.getTreasureOrFalse(1, 2))
                );
    }

    @Test
    void addGroundEntityOrFalse() {
        var map = new MapControllerBasic();
        map.setDimension(3, 3);
        map.addAdventurerOrFalse(0,0, new Adventurer("Anna", null, null));

        assertAll(
                () -> assertTrue(map.addGroundEntityOrFalse(1, 1, new Mountain())),
                () -> assertFalse(map.addGroundEntityOrFalse(1, 1, new Mountain())),
                () -> assertFalse(map.addGroundEntityOrFalse(0, 0, new Mountain())),
                () -> assertThrows(IndexOutOfBoundsException.class, () -> map.addGroundEntityOrFalse(0, 10, new Mountain())),
                () -> assertThrows(IndexOutOfBoundsException.class, () -> map.addGroundEntityOrFalse(10, 0, new Mountain())),
                () -> assertThrows(NullPointerException.class, () -> map.addGroundEntityOrFalse(0, 2, null))
        );
    }

    @Test
    void addAdventurerOrFalse() {
        var map = new MapControllerBasic();
        map.setDimension(3, 3);
        map.addGroundEntityOrFalse(0, 0, new Mountain());
        var ad1 = new Adventurer("Anna", Orientation.SOUTH, new Coordinates(1, 1));
        var ad2 = new Adventurer("Bob", Orientation.SOUTH, new Coordinates(1, 1));
        var ad3 = new Adventurer("Charlie", Orientation.SOUTH, new Coordinates(1, 1));

        assertAll(
                () -> assertTrue(map.addAdventurerOrFalse(1, 1, ad1)),
                () -> assertFalse(map.addAdventurerOrFalse(1, 1, ad1)),
                () -> assertFalse(map.addAdventurerOrFalse(0, 0, ad2)),
                () -> assertThrows(NullPointerException.class, () -> map.addAdventurerOrFalse(1, 0, null)),
                () -> assertThrows(IndexOutOfBoundsException.class, () -> map.addAdventurerOrFalse(1, 1000, ad3)),
                () -> assertThrows(IndexOutOfBoundsException.class, () -> map.addAdventurerOrFalse(-1, 0, ad3))
        );
    }

    @Test
    void moveAdventurerOrFalse() {
        var map = new MapControllerBasic();
        map.setDimension(3, 3);
        map.addGroundEntityOrFalse(2, 2, new Mountain());
        map.addAdventurerOrFalse(1, 1, new Adventurer("Anna", Orientation.SOUTH, new Coordinates(1, 1)));
        map.addAdventurerOrFalse(0, 2, new Adventurer("Bob", Orientation.SOUTH, new Coordinates(0, 2)));
        assertAll(
                () -> assertFalse(map.moveAdventurerOrFalse(0, 0, 0, 0)),
                () -> assertFalse(map.moveAdventurerOrFalse(0, 0, 1, 1)),
                () -> assertFalse(map.moveAdventurerOrFalse(1, 1, 2, 2)),
                () -> assertThrows(IndexOutOfBoundsException.class,() -> map.moveAdventurerOrFalse(-1, 0, 0, 0)),
                () -> assertThrows(IndexOutOfBoundsException.class,() -> map.moveAdventurerOrFalse(0, -1, 0, 0)),
                () -> assertTrue(map.moveAdventurerOrFalse(1, 1, 1, 2)),
                () -> assertFalse(map.moveAdventurerOrFalse(1, 1, 1, 2)),
                () -> assertTrue(map.moveAdventurerOrFalse(1, 2, 0, 0)),
                () -> assertFalse(map.moveAdventurerOrFalse(0, 0, 0, 2)),
                () -> assertTrue(map.moveAdventurerOrFalse(0, 2, 1, 1))
                );
    }
}