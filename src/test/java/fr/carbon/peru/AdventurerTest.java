package fr.carbon.peru;

import fr.carbon.peru.orientation.Orientation;
import org.junit.jupiter.api.Test;

import java.util.Objects;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class AdventurerTest {

    @Test
    void getAndSet() {
        var anna = new Adventurer("Anna", Orientation.SOUTH, new Coordinates(0, 0));
        assertAll(
                () -> assertEquals("Anna", anna.getName()),
                () -> assertEquals(Orientation.SOUTH, anna.getOrientation()),
                () -> assertEquals(new Coordinates(0,0), anna.getCoordinates())
        );
    }

    @Test
    void turnRightAndLeft() {
        var anna = new Adventurer("Anna", Orientation.SOUTH, new Coordinates(0, 0));

        anna.turnRight();
        assertEquals(Orientation.WEST, anna.getOrientation());
        anna.turnRight();
        assertEquals(Orientation.NORTH, anna.getOrientation());
        anna.turnRight();
        assertEquals(Orientation.EAST, anna.getOrientation());
        anna.turnRight();
        assertEquals(Orientation.SOUTH, anna.getOrientation());
        anna.turnLeft();
        assertEquals(Orientation.EAST, anna.getOrientation());
        anna.turnLeft();
        assertEquals(Orientation.NORTH, anna.getOrientation());
        anna.turnLeft();
        assertEquals(Orientation.WEST, anna.getOrientation());
        anna.turnLeft();
        assertEquals(Orientation.SOUTH, anna.getOrientation());
    }

    @Test
    void testForwardAndForward() {
        var anna = new Adventurer("Anna", Orientation.SOUTH, new Coordinates(1, 1));
        assertEquals(new Coordinates(1, 2), anna.testForward());
        anna.turnLeft();
        assertEquals(new Coordinates(2, 1), anna.testForward());
        anna.turnLeft();
        assertEquals(new Coordinates(1, 0), anna.testForward());
        anna.turnLeft();
        assertEquals(new Coordinates(0, 1), anna.testForward());

        anna.forward();
        assertEquals(new Coordinates(0, 1), anna.getCoordinates());
        anna.turnLeft();
        anna.forward();
        assertEquals(new Coordinates(0, 2), anna.getCoordinates());
        anna.turnLeft();
        anna.forward();
        assertEquals(new Coordinates(1, 2), anna.getCoordinates());
        anna.turnLeft();
        anna.forward();
        assertEquals(new Coordinates(1, 1), anna.getCoordinates());
    }

    @Test
    void addAndGetTreasure() {
        var anna = new Adventurer("Anna", Orientation.SOUTH, new Coordinates(0, 0));

        assertEquals(0, anna.getTreasure());
        anna.addTreasure();
        assertEquals(1, anna.getTreasure());
        IntStream.range(0, 9999).forEach(i -> anna.addTreasure());
        assertEquals(10000, anna.getTreasure());
    }
}