package fr.carbon.peru;

import fr.carbon.peru.operator.CommandRecord;
import fr.carbon.peru.operator.CommandControlBasic;
import fr.carbon.peru.operator.command.Forward;
import fr.carbon.peru.operator.command.TurnLeft;
import fr.carbon.peru.operator.command.TurnRight;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class CommandControlBasicTest {

    @Test
    void addAdventurer() {
        var command = new CommandControlBasic();
        assertThrows(NullPointerException.class, () -> command.addAdventurer(null));

        assertEquals(0, command.addAdventurer(new Adventurer("Anna", null, null)));
        IntStream.range(1, 100)
                .forEach(i -> command.addAdventurer(new Adventurer("Anna", null, null)));
        assertEquals(100, command.addAdventurer(new Adventurer("AnnAa", null, null)));
    }

    @Test
    void getAdventurer() {
        var command = new CommandControlBasic();
        var anna = new Adventurer("Anna", null, null);
        assertAll(
                () -> assertThrows(IndexOutOfBoundsException.class, () -> command.getAdventurer(-1)),
                () -> assertThrows(IndexOutOfBoundsException.class, () -> command.getAdventurer(1)),
                () -> assertEquals(0, command.addAdventurer(anna)),
                () -> assertEquals(Optional.of(anna), command.getAdventurer(0))

        );
    }

    @Test
    void addAndGetCommand() {
        var command = new CommandControlBasic();
        var anna = new Adventurer("Anna", null, null);
        var bob = new Adventurer("Bob", null, null);
        command.addAdventurer(anna);
        command.addAdventurer(bob);
        command.addCommand(0, "AGD");
        command.addCommand(1, "GAG");
        assertAll(
                () -> assertThrows(IndexOutOfBoundsException.class, () -> command.addCommand(2, "AA")),
                () -> assertThrows(NullPointerException.class, () -> command.addCommand(0, null)),
                () -> assertThrows(IllegalStateException.class, () -> command.addCommand(0, "AA")),
                () -> assertInstanceOf(Forward.class, command.getNextCommand().get(0)),
                () -> assertInstanceOf(TurnLeft.class, command.getNextCommand().get(0)),
                () -> assertInstanceOf(TurnRight.class, command.getNextCommand().get(0)),
                () -> assertEquals(List.of(), command.getNextCommand())
        );
    }
}